<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop CE
 */
$sMetadataVersion = '2.1';
$aModule = array(
    'id'            => 'NaehrwerteTabelle',
    'title'       => array(
        'de' => '.BEES | OXID6 Nährwerte Tabelle',
        'en' => '.BEES | OXID6 Nutritional Value',
    ),
    'description'  => array(
        'de' => 'Dies ist das Nährwertmodul für den OXID eShop.',
        'en' => 'This is the Nutritional value module for the OXID eShop.'
    ),
    'thumbnail'   => 'out/src/pictures/picture.png',
    'author'      => 'Internetservice | Steffen Winde',
    'url'         => 'https://bitbucket.org/swinde/naehrwertetabelle.git',
    'email'       => 'inserv@winde-ganzig.de',
    'extend'        => array(
    ),
    'controllers'   => array(
        'swinde_naehrwertetabelle' => Swinde\NaehrwerteTabelle\Controller\Admin\NaehrwerteTabelle::class,
    ),
    'templates' => array(
        'swinde_naehrwertetabelle.tpl' => 'swinde/NaehrwerteTabelle/views/admin/tpl/swinde_naehrwertetabelle.tpl'
    ),
    'events' => array(
        'onActivate'   => '\Swinde\NaehrwerteTabelle\Core\Events::onActivate',
        'onDeactivate' => '\Swinde\NaehrwerteTabelle\Core\Events::onDeactivate',
    ),
    'blocks'      => array(
        //Frontend
        //Frontend
        array(
            'theme' => 'moga',
            'template' => 'page/details/inc/tabs.tpl',
            'block'=>'details_tabs_invite',
            'file'=>'views/blocks/page/details/inc/sw_naehrwertetabelle_details_moga.tpl'
        ),
        array(
            'theme' => 'moga',
            'template' => 'page/details/inc/tabs.tpl',
            'block'=>'details_tabs_longdescription',
            'file'=>'views/blocks/page/details/inc/sw_naehrwertetabelle_ingredients_moga.tpl'
        ),
        array(
            'template' => 'page/details/inc/tabs.tpl',
            'block'=>'details_tabs_invite',
            'file'=>'views/blocks/page/details/inc/swinde_naehrwertetabelle_details.tpl'
        ),
        array(
            'template' => 'page/details/inc/tabs.tpl',
            'block'=>'details_tabs_longdescription',
            'file'=>'views/blocks/page/details/inc/swinde_naehrwertetabelle_ingredients.tpl'
        )
    ),
);