Nährwerttabelle für den OXID 6 eShop.
====

### Shopversion
OXID eShop 6


### Installation
`composer config repo.swinde/naehrwertetabelle git https://bitbucket.org/swinde/naehrwertetabelle.git`  
  
`composer require --no-scripts --update-no-dev --no-interaction --optimize-autoloader swinde/naehrwertetabelle`  


### License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.  
