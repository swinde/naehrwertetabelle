[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Steffen Winde | winde-ganzig.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{oxhasrights ident="SHOWLONGDESCRIPTION"}]
    [{assign var="oLongdesc" value=$oDetailsProduct->getLongDescription()}]
    [{if $oLongdesc->value}]
    [{capture append="tabs"}]<a class="nav-link[{if $blFirstTab}] active[{/if}]" href="#description" data-toggle="tab">[{oxmultilang ident="DESCRIPTION"}]</a>[{/capture}]
    [{capture append="tabsContent"}]
    <div id="description" class="tab-pane[{if $blFirstTab}] active[{/if}]" itemprop="description">
        [{oxeval var=$oLongdesc}]
        [{if $oDetailsProduct->oxarticles__oxexturl->value}]
        <a id="productExturl" class="js-external" href="http://[{$oDetailsProduct->oxarticles__oxexturl->value}]">
            [{if $oDetailsProduct->oxarticles__oxurldesc->value}]
            [{$oDetailsProduct->oxarticles__oxurldesc->value}]
            [{else}]
            [{$oDetailsProduct->oxarticles__oxexturl->value}]
            [{/if}]
        </a>
        [{/if}]
        [{if $oDetailsProduct->oxarticles__swingredients->value}]
            <div class="ingredients">
                <p><b>[{oxmultilang ident="INGREDIENTS"}]:</b> [{$oDetailsProduct->oxarticles__swingredients->value|html_entity_decode}]</p>
            </div>
        [{/if}]
    </div>
    [{/capture}]
    [{assign var="blFirstTab" value=false}]
    [{/if}]
[{/oxhasrights}]