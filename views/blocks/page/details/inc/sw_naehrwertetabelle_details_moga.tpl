[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Steffen Winde | winde-ganzig.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{$smarty.block.parent}]
[{block name="details_tabs_naehrwerte"}]
    [{if $oDetailsProduct->oxarticles__swcaloric1->value}]
        [{capture append="tabs"}]
            <a id="naehrwerte-tab" class="nav-link[{if $blFirstTab}] active[{/if}]" href="#naehrwerte" data-bs-toggle="tab" role="tab" aria-controls="naehrwerte" aria-selected="false">
                    [{oxmultilang ident="NAEHRWERTE_TAB_TITLE"}]</a>
        [{/capture}]
        [{capture append="tabsContent"}]
        <div id="naehrwerte" class="tab-pane[{if $blFirstTab}] active[{/if}]" role="tabpanel" aria-labelledby="naehrwerte-tab"">
            [{block name="naehrwerte_productmain"}]
            <div class="container-xxl naehrwerte table-responsive">
                <h5>[{oxmultilang ident="NAEHRWERTE_SHORTDISCRIPTION"}]</h5>
                [{*oxmultilang ident="ABTROPFGEWICHT"}][{$oDetailsProduct->oxarticles__swabtropfgewicht->value}][{ oxmultilang ident="UNIT_G" *}]
                <table class="table naehrwerte">
                    <tbody>
                    [{if $oDetailsProduct->oxarticles__swcaloric1->value}]
                        <tr class="level_1">
                            <td>[{oxmultilang ident="BRENNWERT"}]</td>
                            <td><strong>[{$oDetailsProduct->oxarticles__swcaloric1->value}] [{oxmultilang ident="UNIT_BRENNWERT_1"}]</strong> ([{$oDetailsProduct->oxarticles__swcaloric2->value}] [{ oxmultilang ident="UNIT_BRENNWERT_2" }]) </td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swfat1->value}]
                        <tr class="level_1">
                            <td>[{oxmultilang ident="FETT"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swfat1->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swfat2->value}]
                        <tr class="level_2">
                            <td>[{oxmultilang ident="GESAETTIGTE_FETTSAEUREN"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swfat2->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swfat3->value}]
                        <tr class="level_2">
                            <td>[{oxmultilang ident="UNGESAETTIGTE_FETTSAEUREN"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swfat3->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swcarb->value}]
                        <tr class="level_1">
                            <td>[{oxmultilang ident="KOHLENHYDRATE"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swcarb->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swsugar->value}]
                        <tr class="level_2">
                            <td>[{oxmultilang ident="ZUCKER"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swsugar->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swprotein->value}]
                        <tr class="level_1">
                            <td>[{oxmultilang ident="EIWEISS"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swprotein->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    [{if $oDetailsProduct->oxarticles__swsalt->value}]
                        <tr class="level_1">
                            <td>[{oxmultilang ident="SALZ"}]</td>
                            <td>[{$oDetailsProduct->oxarticles__swsalt->value}] [{ oxmultilang ident="UNIT_G" }]</td>
                        </tr>
                        [{/if}]
                    </tbody>
                </table>
            </div>
            [{/block}]
        </div>

        [{/capture}]
        [{assign var="blFirstTab" value=false}]
    [{/if}]
[{/block}]