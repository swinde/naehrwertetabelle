<?php

/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Steffen Winde | winde-ganzig.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
namespace Swinde\NaehrwerteTabelle\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;

class NaehrwerteTabelle extends AdminController
{
    protected $_sThisTemplate = "swinde_naehrwertetabelle.tpl";

    /**
     * @return bool|string
     */

    public function render()
    {
        parent::render();
        $this->_aViewData['edit'] = $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        $soxId = $this->_aViewData["oxid"] = $this->getEditObjectId();

        if (  $soxId && $soxId != "-1")
        {
            // load object
            $oArticle->loadInLang( $this->_iEditLang, $soxId );
        }
        $oDB = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        $sArticleTable = getViewName( 'oxarticles', $this->_iEditLang );
        $sSelect  = " select $sArticleTable.swcaloric1, $sArticleTable.swcaloric2, $sArticleTable.swfat1, $sArticleTable.swfat2, $sArticleTable.swfat3, $sArticleTable.swcarb, $sArticleTable.swsugar, $sArticleTable.swprotein, $sArticleTable.swsalt, $sArticleTable.swingredients  from $sArticleTable";
        $sSelect .= " where $sArticleTable.oxid = '".$soxId."'";


        $resultSet = \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->select($sSelect);
        //Fetch the results row by row
        if ($resultSet != false && $resultSet->count() > 0) {
            while (!$resultSet->EOF) {
                $row = $resultSet->getFields();
                //do something
                $swcaloric1 = new \OxidEsales\Eshop\Core\Field();
                $swcaloric2 = new \OxidEsales\Eshop\Core\Field();
                $swfat1     = new \OxidEsales\Eshop\Core\Field();
                $swfat2     = new \OxidEsales\Eshop\Core\Field();
                $swfat3     = new \OxidEsales\Eshop\Core\Field();
                $swcarb     = new \OxidEsales\Eshop\Core\Field();
                $swsugar    = new \OxidEsales\Eshop\Core\Field();
                $swprotein  = new \OxidEsales\Eshop\Core\Field();
                $swsalt  = new \OxidEsales\Eshop\Core\Field();
                $swabtropfgewicht  = new \OxidEsales\Eshop\Core\Field();
                $swingredients  = new \OxidEsales\Eshop\Core\Field();
                $resultSet->fetchRow();
            }
        }
        $this->_aViewData['swcaloric1'] = $swcaloric1;
        $this->_aViewData['swcaloric2'] = $swcaloric2;
        $this->_aViewData['swfat1'] = $swfat1;
        $this->_aViewData['swfat2'] = $swfat2;
        $this->_aViewData['swfat3'] = $swfat3;
        $this->_aViewData['swcarb'] = $swcarb;
        $this->_aViewData['swsugar'] = $swsugar;
        $this->_aViewData['swprotein'] = $swprotein;
        $this->_aViewData['swsalt'] = $swsalt;
        $this->_aViewData['swabtropfgewicht'] = $swabtropfgewicht;
        $this->_aViewData['swingredients'] = $swingredients;

        return 'swinde_naehrwertetabelle.tpl';
    }

    /**
     * Saves changes of article parameters.
     */
    /**
     * Saves content contents.
     *
     * @return mixed
     */
    public function save()
    {
        parent::save();

        $oConfig = $this->getConfig();
        $soxId = $this->getEditObjectId();
        $aParams = $this->getConfig()->getRequestParameter("editval");

        $soxparentId = $oConfig->getRequestParameter( "oxparentid");
        if ( isset( $soxparentId) && $soxparentId && $soxparentId != "-1")
        {
            unset( $aParams['oxarticles__oxparentid']);
        }

        $oArticle = oxNew( "oxarticle");
        $oArticle->setLanguage($this->_iEditLang);

        if ( $soxId != "-1") {
            $oArticle->loadInLang( $this->_iEditLang, $soxId);
        }
        $oArticle->setLanguage(0);

        $oArticle->assign( $aParams );
        //$oArticle->setArticleLongDesc( $this->_processLongDesc( $aParams['oxarticles__oxlongdesc'] ) );
        $oArticle->setLanguage($this->_iEditLang);
        //$oArticle = oxRegistry::get("oxUtilsFile")->processFiles( $oArticle );
        $oArticle->save();

        $this->setEditObjectId( $oArticle->getId() );
    }
}
