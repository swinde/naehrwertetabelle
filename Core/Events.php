<?php

/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Steffen Winde | winde-ganzig.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Swinde\NaehrwerteTabelle\Core;

use OxidEsales\Eshop\Core\Registry;

class Events
{
    /**
     * Add additional fields: Naehrwertetable
     */
    public static function onActivate()
    {
        if( !self::_swCheckIfDatabaseFieldExists() ) {
            self::swAlterArticlesTable();
        }

    }

    public static function swAlterArticlesTable ()
    {
        $query = "ALTER TABLE `oxarticles` ADD (
                `SWCALORIC1` varchar(40) NOT NULL COMMENT 'Feld für Brennwert (in kJ)',
                `SWCALORIC2` varchar(40) NOT NULL COMMENT 'Feld für Brennwert (in Kcal)',
                `SWFAT1` varchar(40) NOT NULL COMMENT 'Feld für Fettgehalt (in g)',
                `SWFAT2` varchar(40) NOT NULL COMMENT 'Feld für ungesättigte Fettsaeuren (in g)',
                `SWFAT3` varchar(40) NOT NULL COMMENT 'Feld für gesättigte Fettsaeuren (in g)',
                `SWCARB` varchar(40) NOT NULL COMMENT 'Feld für Kohlehydrate (in g)',
                `SWSUGAR` varchar(40) NOT NULL COMMENT 'Feld für Kohlehydrate Zucker (in g)',
                `SWPROTEIN` varchar(40) NOT NULL COMMENT 'Feld für Kohlehydrate Eiweiss (in g)',
                `SWSALT` varchar(40) NOT NULL COMMENT 'Feld für Salz (in g)',
                `SWABTROPFGEWICHT` varchar(40) NOT NULL COMMENT 'Gewicht ohne Aufguss (in g)',
                `SWINGREDIENTS` text NOT NULL COMMENT 'Zutatenliste'                       
            ) ";

        \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->execute($query);
    }

    public static function clearTmp($sClearFolderPath = '')
    {
        $sFolderPath = self::_getFolderToClear($sClearFolderPath);
        $hDirHandler = opendir($sFolderPath);
        if (!empty($hDirHandler)) {
            while (false !== ($sFileName = readdir($hDirHandler))) {
                $sFilePath = $sFolderPath . DIRECTORY_SEPARATOR . $sFileName;
                self::_clear($sFileName, $sFilePath);
            }
            closedir($hDirHandler);
        }
        return true;
    }

    protected static function _getFolderToClear($sClearFolderPath = '')
    {
        $sTempFolderPath = (string) Registry::getConfig()->getConfigParam('sCompileDir');
        if (!empty($sClearFolderPath) and (strpos($sClearFolderPath, $sTempFolderPath) !== false)) {
            $sFolderPath = $sClearFolderPath;
        } else {
            $sFolderPath = $sTempFolderPath;
        }
        return $sFolderPath;
    }

    protected static function _clear($sFileName, $sFilePath)
    {
        if (!in_array($sFileName, ['.', '..', '.gitkeep', '.htaccess'])) {
            if (is_file($sFilePath)) {
                @unlink($sFilePath);
            } else {
                self::clearTmp($sFilePath);
            }
        }
    }
    protected static function _swCheckIfDatabaseFieldExists()
    {
        $blSwDatabaseFieldExists = false;

        $sTable = 'oxarticles';
        $sColumn = 'swcaloric1';
        $oDbHandler = oxNew( "oxDbMetaDataHandler" );
        $blSwDatabaseFieldExists = $oDbHandler->tableExists( $sTable ) && $oDbHandler->fieldExists( $sColumn, $sTable );

        return $blSwDatabaseFieldExists;
    }

    public static function onDeactivate()
    {
        self::clearTmp();
    }
}
